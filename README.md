## cow
Basic Copy-on-Write library.

### Install

```bash
  $ cmake -DCMAKE_BUILD_TYPE=<Debug/Release> -DCMAKE_INSTALL_PREFIX=/path/to/desired/location
```
